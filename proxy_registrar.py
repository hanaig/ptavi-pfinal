import socketserver
import socket
import sys
import json
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import crearLog

datos = []


class SmallSMILHandler(ContentHandler):
    # así leo el fichero xml

    def __init__(self):
        super().__init__()
        self.lista = {}
        self.etiquetas = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwdpath"],
            "log": ["path"],
        }

    def startElement(self, name, attrs):

        if name in self.etiquetas:
            for atributo in self.etiquetas[name]:
                self.lista[name + '-' + atributo] = attrs.get(atributo, "")

    def get_tags(self):
        return self.lista


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Para registrar y borrar del diccionario """
    diccionario = {}

    def meterUsuario(self, dirSip1, tiempoExpires, expires, puerto_clien):
        """ registrar usuarios en el diccionario """
        ipClient, puertoClient = self.client_address  # las devuelve el socket
        self.diccionario[dirSip1] = {'address': ipClient,
                                     'puerto': puerto_clien,
                                     'expires': tiempoExpires,
                                     'tiempo Expiracion': expires}
        print(self.diccionario)
        respuesta = b'SIP/2.0 200 Ok\r\n\r\n'
        self.wfile.write(respuesta)
        log.enviarLog(ipClient, puertoClient, respuesta.decode('utf-8'))

    def borrarUsuario(self, dirSip2):
        """ borrar usuarios del diccionario """
        ipClient, puertoClient = self.client_address
        try:
            del self.diccionario[dirSip2]
            respuesta = b'SIP/2.0 200 Ok\r\n\r\n'
            self.wfile.write(respuesta)
            log.enviarLog(ipClient, puertoClient, respuesta.decode('utf-8'))
        except KeyError:
            self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
        print(self.diccionario)

    def register2json(self):
        """ crear el archivo Json """
        with open("registered.json", 'w') as f:
            json.dump(self.diccionario, f, indent=2)

    def caducidadExpires(self):
        """ comprobar el tiempo expires de los usuarios """
        listaUsuarios = list(self.diccionario)
        for usuario in listaUsuarios:
            tiempoExpires = self.diccionario[usuario]["expires"]
            tiempoReal = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                       time.localtime())
            if tiempoExpires < tiempoReal:
                del self.diccionario[usuario]
        self.register2json()

    def json2registered(self):
        """ comporobar si exixte fichero json """
        try:
            with open('registered.json', 'r') as ficheroJson:
                self.diccionario = json.load(ficheroJson)
        except FileNotFoundError:
            pass

    def enviarServ(self, mensajeCliente, usuario):
        # ahora lo que vamos a enviar al servidor
        # Creo el socket y lo ato al servidor
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            try:
                ip = self.diccionario[usuario]['address']
                puerto = int(self.diccionario[usuario]['puerto'])
                my_socket.connect((ip, puerto))
                mensaje = mensajeCliente.split('\r\n\r\n')
                mensajealServ = mensaje[0] + '\r\n' + 'Via: SIP/2.0/UDP' \
                                           + ipProxy
                mensajealServ = mensajealServ + ':' + str(puertoProxy) \
                                              + '\r\n'
                mensajealServ = mensajealServ + 'User-Agent: ' \
                                              + nombreServidor + '\r\n\r\n'
                mensajealServ = mensajealServ + mensaje[1]
                log.enviarLog(ip, puerto, mensajealServ)
                print("Enviando: " + mensajealServ)
                my_socket.send(bytes(mensajealServ, 'utf-8') + b'\r\n\r\n')
                data = my_socket.recv(1024)
                print('Recibido -- ', data.decode('utf-8'))
                self.wfile.write(data)
                log.recibirLog(ip, puerto, data.decode('utf-8'))
            except ConnectionRefusedError:
                sys.exit('')
            print("Socket terminado")

    def handle(self):
        global datos
        global dirSip
        self.json2registered()
        line = self.rfile.read()
        mensajeCliente = line.decode('utf-8')
        print(mensajeCliente)
        if mensajeCliente != '\r\n':
            print("El cliente nos manda ", mensajeCliente)
            datosCliente = mensajeCliente.split()
            print(datosCliente)
            metodo = datosCliente[0]
            peticion = datosCliente[1].split(':')[0]
            version = datosCliente[2]
            ipClient, puertoClient = self.client_address
            log.recibirLog(ipClient, puertoClient, mensajeCliente)
            if peticion != 'sip' or version != 'SIP/2.0':
                respuesta = b'Bad Request\r\n\r\n'
                self.wfile.write(respuesta)
                log.enviarLog(ipClient, puertoClient,
                              respuesta.decode('utf-8'))
            else:
                if metodo == 'REGISTER':
                    print(datosCliente)
                    dirSip = datosCliente[1].split(':')[1]
                    puerto_clien = datosCliente[1].split(':')[2]
                    expires = datosCliente[4]
                    if datosCliente[-2] == 'Expires:':
                        tiempoExpires = int(datosCliente[-1])
                    if tiempoExpires == 0:
                        self.borrarUsuario(dirSip)
                    elif tiempoExpires > 0:
                        tiempoExpires = tiempoExpires + time.time()
                        tiempoExpires = time.strftime(
                            '%Y-%m-%d %H:%M:%S +0000',
                            time.localtime(tiempoExpires))
                        self.meterUsuario(dirSip, tiempoExpires,
                                          expires, puerto_clien)
                    self.caducidadExpires()
                elif metodo == 'INVITE' or 'BYE' or 'ACK':
                    usuario = mensajeCliente[mensajeCliente.index("sip:")
                                             + 4:
                                             mensajeCliente.index(" SIP/2.0")]
                    if usuario in self.diccionario:
                        self.enviarServ(mensajeCliente, usuario)
                    else:
                        respuesta = b'SIP/2.0 404 User Not Found\r\n\r\n'
                        self.wfile.write(respuesta)
                        log.enviarLog(ipClient, puertoClient,
                                      respuesta.decode('utf-8'))
                else:
                    respuesta = b'Method Not Allowed\r\n\r\n'
                    self.wfile.write(respuesta)
                    log.enviarLog(ipClient, puertoClient,
                                  respuesta.decode('utf-8'))


if __name__ == "__main__":

    try:
        # aqui recojo la informacion desde terminal del archivo ua2.xml
        config = sys.argv[1]
    except (IndexError, ValueError):
        sys.exit('Usage: python proxy_registrar.py config')
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))
    info = cHandler.get_tags()

    # defino la info apartir del archivo
    nombreServidor = info['server-name']
    ipProxy = info['server-ip']
    puertoProxy = int(info['server-puerto'])
    base = info['database-path']
    contrasenas = info['database-passwdpath']
    log1 = info['log-path']

    serv = socketserver.UDPServer((ipProxy, puertoProxy), SIPRegisterHandler)

    print("Server MiServidorBigBang listening at port "
          + str(puertoProxy) + "...")
    try:
        log = crearLog()
        estado = 'inicio'
        log.principioFin(estado)
        serv.serve_forever()
    except KeyboardInterrupt:
        estado = 'fin'
        log.principioFin(estado)
        print("Finalizado servidor")
