import socketserver
import sys
import simplertp
import random
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import crearLog

datos = []


class SmallSMILHandler(ContentHandler):
    # así leo el fichero xml

    def __init__(self):
        super().__init__()
        self.lista = {}
        self.etiquetas = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, name, attrs):

        if name in self.etiquetas:
            for atributo in self.etiquetas[name]:
                self.lista[name + '-' + atributo] = attrs.get(atributo, "")

    def get_tags(self):
        return self.lista


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    diccRtp = {}

    def handle(self):
        global datos

        line = self.rfile.read()
        mensajeCliente = line.decode('utf-8')
        if mensajeCliente != '\r\n':
            print("El cliente nos manda ", mensajeCliente)
            datosCliente = mensajeCliente.split()
            print(datosCliente)
            metodo = datosCliente[0]
            peticion = datosCliente[1].split(':')[0]
            version = datosCliente[2]
            ipclient, puertoclient = self.client_address
            log.recibirLog(ipclient, puertoclient, mensajeCliente)
            if peticion != 'sip' or version != 'SIP/2.0':
                respuesta = b'Bad Request\r\n\r\n'
                self.wfile.write(respuesta)
            else:
                if metodo == 'INVITE':
                    respuesta = \
                        b'SIP/2.0 100 ' \
                        b'Trying\r\n\r\nSIP/2.0 180 ' \
                        b'Ringing\r\n\r\nSIP/2.0 ' \
                        b'200 Ok\r\n'
                    self.wfile.write(respuesta)
                    log.enviarLog(ipclient, puertoclient,
                                  respuesta.decode('utf-8'))
                    ipRecibida = datosCliente[-6]
                    puertoRecibido = datosCliente[-2]
                    self.diccRtp['datosOrigen'] = {'ip': ipRecibida,
                                                   'puerto': puertoRecibido}
                    sdp = 'Content-Type: application/sdp\r\n'
                    sdp = sdp + ('Content-Lenght: ' + str(len('v=0\r\n' + 'o='
                                                              + usuario + ' '
                                                              + ipserv
                                                              + '\r\n'
                                                              + 's='
                                                              + 'miSesion'
                                                              + '\r\n'
                                                              + 't=' + '0'
                                                              + '\r\n'
                                                              + 'm='
                                                              + 'audio '
                                                              + puertortp
                                                              + ' ' + 'RTP'
                                                              + '\r\n'))
                                 + '\r\n\r\n')
                    sdp = sdp + 'v=0\r\n'
                    sdp = sdp + ('o=' + usuario + ' ' + ipserv + '\r\n')
                    sdp = sdp + ('s=' + 'miSesion' + '\r\n')
                    sdp = sdp + ('t=' + '0' + '\r\n')
                    sdp = sdp + ('m=' + 'audio ' + str(puertortp)
                                 + ' ' + 'RTP' + '\r\n')
                    self.wfile.write(bytes(sdp, 'utf-8'))
                    log.enviarLog(ipclient, puertoclient, sdp)
                    datos = datosCliente
                elif metodo == 'BYE':
                    respuesta = b'SIP/2.0 200 Ok\r\n\r\n'
                    self.wfile.write(respuesta)
                    log.enviarLog(ipclient, puertoclient,
                                  respuesta.decode('utf-8'))
                elif metodo == 'ACK':
                    global audio
                    ALEAT = int(random.random()
                                * 100000)
                    BIT = int(random.randrange(0, 1))
                    ip = self.diccRtp['datosOrigen']['ip']
                    port = int(self.diccRtp['datosOrigen']['puerto'])
                    n = random.randrange(0, 15)
                    csrc = [random.randint(0, 2 ** 32) for _ in range(n)]
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(pad_flag=0,
                                          ext_flag=0,
                                          cc=len(csrc), marker=BIT,
                                          ssrc=ALEAT)
                    RTP_header.setCSRC(csrc)
                    audio = simplertp.RtpPayloadMp3(audio1)
                    simplertp.send_rtp_packet(RTP_header, audio, ip, port)
                    log.paquetesRtp(ip, port, audio1)
                else:
                    respuesta = b'Method Not Allowed\r\n\r\n'
                    self.wfile.write(respuesta)
                    log.enviarLog(ipclient, puertoclient,
                                  respuesta.decode('utf-8'))


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos

    try:
        try:
            # aqui recojo la informacion desde terminal del archivo ua2.xml
            config = sys.argv[1]
        except (IndexError, ValueError):
            sys.exit('Usage: python uaserver.py config')
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(config))
        infoserv = cHandler.get_tags()

        usuario = infoserv['account-username']
        contrasena = infoserv['account-passwd']
        ipserv = infoserv['uaserver-ip']
        puertoserv = int(infoserv['uaserver-puerto'])
        puertortp = infoserv['rtpaudio-puerto']
        ipproxy = infoserv['regproxy-ip']
        puertoproxy = int(infoserv['regproxy-puerto'])
        log1 = infoserv['log-path']
        audio1 = infoserv['audio-path']
        serv = socketserver.UDPServer((ipserv, puertoserv), EchoHandler)

        try:
            print("Listening...")
            log = crearLog()
            estado = 'inicio'
            log.principioFin(estado)
            serv.serve_forever()
        except KeyboardInterrupt:
            estado = 'fin'
            log.principioFin(estado)
            print("Finalizado")
    except (IndexError, ValueError):
        sys.exit('Usage: python3 server.py IP port audio_file')
