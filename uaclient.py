import socket
import sys
import simplertp
import random
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time


class SmallSMILHandler(ContentHandler):
    # así leo el fichero xml

    def __init__(self):
        super().__init__()
        self.lista = {}
        self.etiquetas = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, name, attrs):

        if name in self.etiquetas:
            for atributo in self.etiquetas[name]:
                self.lista[name + '-' + atributo] = attrs.get(atributo, "")

    def get_tags(self):
        return self.lista


class crearLog:

    def __init__(self):

        from __main__ import log1 as log
        self.log = log

    def escribirLog(self, mensaje):
        with open(self.log, 'a') as ficheroLog:
            ficheroLog.write(mensaje)

    def principioFin(self, estado):
        real_time = time.strftime('%Y%m%d%H%M%S ',
                                  time.gmtime(time.time()))
        if estado == 'inicio':
            mensaje = real_time + 'starting...' + '\r\n'
            self.escribirLog(mensaje)
        elif estado == 'fin':
            mensaje = real_time + 'Finishing.' + '\r\n'
            self.escribirLog(mensaje)

    def enviarLog(self, ip, puerto, mens):
        real_time = time.strftime('%Y%m%d%H%M%S ',
                                  time.gmtime(time.time()))
        mens = mens.replace('\r\n', ' ')
        mensaje = (real_time + 'Sent to ' + ip
                   + ':' + str(puerto) + ': ' + mens + '\r\n')
        self.escribirLog(mensaje)

    def recibirLog(self, ip, puerto, mens):
        real_time = time.strftime('%Y%m%d%H%M%S ',
                                  time.gmtime(time.time()))
        mens = mens.replace('\r\n', ' ')
        mensaje = (real_time + 'Received from ' + ip
                   + ':' + str(puerto) + ': ' + mens + '\r\n')
        self.escribirLog(mensaje)

    def errorConex(self):
        real_time = time.strftime('%Y%m%d%H%M%S ',
                                  time.gmtime(time.time()))
        mensaje = (real_time + '20101018160243 Error: No server listening at '
                   + ipproxy + ' port ' + str(puertoproxy) + '\r\n')
        self.escribirLog(mensaje)

    def paquetesRtp(self, ip, puerto, audio):
        real_time = time.strftime('%Y%m%d%H%M%S ',
                                  time.gmtime(time.time()))
        mensaje = (real_time + ' Sending to ' + ip
                   + ':' + str(puerto) + ' ' + audio + ' by RTP\r\n')
        self.escribirLog(mensaje)


if __name__ == "__main__":
    try:
        # aqui recojo la informacion desde terminal
        config = sys.argv[1]
        metodo = sys.argv[2]
        expires = sys.argv[3]
    except (IndexError, ValueError):
        sys.exit('Usage: python uaclient.py config method option')

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))
    infoArchivo = cHandler.get_tags()
    print(infoArchivo)

    # extraigo los parametros que voy a necesitar
    usuario = infoArchivo['account-username']
    contrasena = infoArchivo['account-passwd']
    ipcli = infoArchivo['uaserver-ip']
    puertocli = infoArchivo['uaserver-puerto']
    puertortp = infoArchivo['rtpaudio-puerto']
    ipproxy = infoArchivo['regproxy-ip']
    puertoproxy = int(infoArchivo['regproxy-puerto'])
    log1 = infoArchivo['log-path']
    audio1 = infoArchivo['audio-path']

    log = crearLog()
    estado = 'inicio'
    log.principioFin(estado)

    # ahora miro los metodos a enviar
    if metodo == 'INVITE':
        line = ('INVITE ' + 'sip:' + expires + ' SIP/2.0\r\n')
        line = line + 'Content-Type: application/sdp\r\n'
        line = line + ('Content-Lenght: ' + str(len('v=0\r\n' + 'o='
                                                    + usuario + '@'
                                                    + 'bigbang.org '
                                                    + ipcli + '\r\n' + 's='
                                                    + 'miSesion' + '\r\n'
                                                    + 't=' + '0' + '\r\n'
                                                    + 'm=' + 'audio '
                                                    + puertortp
                                                    + ' ' + 'RTP'
                                                    + '\r\n')) + '\r\n\r\n')
        line = line + 'v=0\r\n'
        line = line + ('o=' + usuario + '@' + 'bigbang.org ' + ipcli + '\r\n')
        line = line + ('s=' + 'miSesion' + '\r\n')
        line = line + ('t=' + '0' + '\r\n')
        line = line + ('m=' + 'audio ' + puertortp + ' ' + 'RTP' + '\r\n')
    elif metodo == 'BYE':
        line = ('BYE ' + 'sip:' + expires + ' SIP/2.0\r\n')
    elif metodo == 'REGISTER':
        line = ('REGISTER ' + 'sip:' + usuario + ':' + puertocli
                + ' SIP/2.0\r\n' + 'Expires: ' + expires + '\r\n')
    else:
        line = metodo

    # Contenido que vamos a enviar
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        try:
            my_socket.connect((ipproxy, puertoproxy))
            print("Enviando: ", line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')
            log.enviarLog(ipproxy, puertoproxy, line)
            data = my_socket.recv(1024)
            respuestaServidor = data.decode('utf-8')
            print('Recibido -- ', respuestaServidor)
            log.recibirLog(ipproxy, puertoproxy, respuestaServidor)
        except ConnectionRefusedError:
            log.errorConex()
            sys.exit("20101018160243 Error: No server listening at "
                     + ipproxy + "port " + str(puertoproxy))

        # revisar cuando el servidor manda el ack y lo que tiene q hacer
        if metodo == 'INVITE':
            try:
                respuestaServidor1 = respuestaServidor.split('\r\n\r\n')
                log.recibirLog(ipproxy, puertoproxy, respuestaServidor)
                respuestaServidor = respuestaServidor1[2].split('\r\n')
                print(respuestaServidor)
                respuestaServidor = respuestaServidor[0]
                if respuestaServidor == 'SIP/2.0 200 Ok':
                    line = ('ACK' + ' sip:' + expires + ' SIP/2.0\r\n')
                    my_socket.send(bytes(line, 'utf-8') + b'\r\n')
                    log.enviarLog(ipproxy, puertoproxy, line)
                    ip1 = respuestaServidor1[3].split('\r\n')
                    ip = ip1[1].split()
                    ip = ip[1]
                    port = ip1[4].split()
                    port = port[1]
                    port = int(port)
                    ALEAT = int(random.random()
                                * 100000)
                    BIT = int(random.randrange(0, 1))
                    n = random.randrange(0, 15)
                    csrc = [random.randint(0, 2 ** 32) for _ in range(n)]
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(pad_flag=0,
                                          ext_flag=0,
                                          cc=len(csrc), marker=BIT,
                                          ssrc=ALEAT)
                    RTP_header.setCSRC(csrc)
                    audio = simplertp.RtpPayloadMp3(audio1)
                    simplertp.send_rtp_packet(RTP_header, audio, ip, port)
                    log.paquetesRtp(ip, port, audio1)
            except Exception:
                sys.exit('')
    print("Terminando socket...")
    print("Fin.")
    estado = 'fin'
    log.principioFin(estado)
